package com.acquiring.app.classifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TestOnNokiaData();
    }

    public static void TestonOurData() {
        String path = "C:\\Users\\mohamed\\Downloads\\Assets\\50HZ\\";
        Classifier classifier = new Classifier();
        classifier.parameters = 3;
        int id = 1;
        for (int j = 0; j < 6; j++) {
            Symbols s = new Symbols();
            s.id = j;
            for (int i = 1; i <= 3; i++) {
                String FileName = String.format("accOut_%d.txt", id++);
                String fullPath = path + FileName;
                Scanner input = new Scanner(System.in);
                File file = new File(fullPath);
                try {
                    input = new Scanner(file);
                    ArrayList<ArrayList<Float>> readedData = new ArrayList<ArrayList<Float>>();
                    int z = 0;
                    while (input.hasNext()) {
                        ArrayList<Float> ins = new ArrayList<Float>();
                        String line = input.nextLine();
                        if (z++ == 0) continue;
                        for (String temp : line.split(",")) {
                            ins.add(Float.parseFloat(temp));
                        }

                        readedData.add(ins);
                    }
                    s.refrenceData.add(readedData);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            classifier.TrainiedData.add(s);
        }
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                pair p = classifier.Classifiy(classifier.TrainiedData.get(i).refrenceData.get(j));
                System.out.printf("%d %d\n", i, p.ID);
            }
        }
    }

    public static void TestOnNokiaData() {
        Classifier classifier = new Classifier();
        classifier.parameters = 3;
        for (int day = 1; day <= 1; day++) {
            for (int id = 1; id <= 8; id++) {
                Symbols s = new Symbols();
                s.id = id;
                for (int Try = 1; Try <= 10; Try++) {
                    String fullPath = String.format("H:\\accData\\U1 (%d)\\A_Template_Acceleration%d-%d.txt", day, id, Try);
                    Scanner input = new Scanner(System.in);
                    File file = new File(fullPath);
                    try {
                        input = new Scanner(file);
                        ArrayList<ArrayList<Float>> readedData = new ArrayList<ArrayList<Float>>();
                        int z = 0;
                        while (input.hasNext()) {
                            ArrayList<Float> ins = new ArrayList<Float>();
                            String line = input.nextLine();
                            if (z++ == 0) continue;
                            for (String temp : line.split(" ")) {
                                ins.add(Float.parseFloat(temp));
                            }

                            readedData.add(ins);
                        }
                        s.refrenceData.add(readedData);
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                classifier.TrainiedData.add(s);
            }
        }
        int total = 0, correct = 0;
        for (int day = 1; day <= 7; day++) {
            for (int id = 1; id <= 8; id++) {
                for (int Try = 1; Try <= 10; Try++) {
                    total++;
                    String fullPath = String.format("H:\\accData\\U1 (%d)\\A_Template_Acceleration%d-%d.txt", day, id, Try);
                    Scanner input = new Scanner(System.in);
                    File file = new File(fullPath);
                    try {
                        input = new Scanner(file);
                        ArrayList<ArrayList<Float>> readedData = new ArrayList<ArrayList<Float>>();
                        int z = 0;
                        while (input.hasNext()) {
                            ArrayList<Float> ins = new ArrayList<Float>();
                            String line = input.nextLine();
                            if (z++ == 0) continue;
                            for (String temp : line.split(" ")) {
                                ins.add(Float.parseFloat(temp));
                            }

                            readedData.add(ins);
                        }
                        pair p = classifier.Classifiy(readedData);
                        if (p.ID == id) {
                            correct++;
                        }
                        System.out.printf("%d %d\n", total, correct);
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
        }
    }
}
