package com.acquiring.app.classifier;

import java.util.ArrayList;

public class DTW {
    public static final double VERY_BIG = 1e30;

    public float dtw(ArrayList<ArrayList<Float>> x, ArrayList<ArrayList<Float>> y, int params) {

        double[][] globdist;
        double[][] Dist;

        double top;
        double mid;
        double bot;
        double cheapest;
        double total;
        short[][] move;
        short[][] warp;
        short[][] temp;
        int I;
        int X;
        int Y;
        int n;
        int i;
        int j;
        int k;
        int xsize;
        int ysize;
        xsize = x.size();
        ysize = y.size();
        /* allocate memory for Dist */
        Dist = new double[xsize][];
        globdist = new double[xsize][];
        move = new short[xsize][];
        temp = new short[xsize * 3][];
        warp = new short[xsize * 3][];
        for (i = 0; i < xsize; i++) {
            Dist[i] = new double[ysize];
            globdist[i] = new double[ysize];
            move[i] = new short[ysize];
        }
        for (i = 0; i < xsize * 3; i++) {
            temp[i] = new short[3];
            warp[i] = new short[3];
        }
        for (i = 0; i < xsize; i++) {
            for (j = 0; j < ysize; j++) {
                total = 0;
                for (k = 0; k < params; k++) {
                    total = total + ((x.get(i).get(k) - y.get(j).get(k)) * (x.get(i).get(k) - y.get(j).get(k)));
                }
                Dist[i][j] = total;
            }
        }
        globdist[0][0] = Dist[0][0];
        for (j = 1; j < xsize; j++) {
            globdist[j][0] = VERY_BIG;
        }

        globdist[0][1] = VERY_BIG;
        globdist[1][1] = globdist[0][0] + Dist[1][1];
        move[1][1] = 2;

        for (j = 2; j < xsize; j++) {
            globdist[j][1] = VERY_BIG;
        }

        for (i = 2; i < ysize; i++) {
            globdist[0][i] = VERY_BIG;
            globdist[1][i] = globdist[0][i - 1] + Dist[1][i];

            for (j = 2; j < xsize; j++) {
                top = globdist[j - 1][i - 2] + Dist[j][i - 1] + Dist[j][i];
                mid = globdist[j - 1][i - 1] + Dist[j][i];
                bot = globdist[j - 2][i - 1] + Dist[j - 1][i] + Dist[j][i];
                if ((top < mid) && (top < bot)) {
                    cheapest = top;
                    I = 1;
                } else if (mid < bot) {
                    cheapest = mid;
                    I = 2;
                } else {
                    cheapest = bot;
                    I = 3;
                }

				/*if all costs are equal, pick middle path*/
                if ((top == mid) && (mid == bot)) {
                    I = 2;
                }

                globdist[j][i] = cheapest;
                move[j][i] = (short) I;
            }
        }
        X = ysize - 1;
        Y = xsize - 1;
        n = 0;
        warp[n][0] = (short) X;
        warp[n][1] = (short) Y;
        while (X > 0 && Y > 0 && n + 1 < 3 * xsize) {
            n = n + 1;
            if (move[Y][X] == 1) {
                warp[n][0] = (short) (X - 1);
                warp[n][1] = (short) Y;
                n = n + 1;
                X = X - 2;
                Y = Y - 1;
            } else if (move[Y][X] == 2) {
                X = X - 1;
                Y = Y - 1;
            } else if (move[Y][X] == 3) {
                warp[n][0] = (short) X;
                warp[n][1] = (short) (Y - 1);
                n = n + 1;
                X = X - 1;
                Y = Y - 2;
            }
            warp[n][0] = (short) X;
            warp[n][1] = (short) Y;

        }
        for (i = 0; i <= n; i++) {
            temp[i][0] = warp[n - i][0];
            temp[i][1] = warp[n - i][1];

        }
        for (i = 0; i <= n; i++) {
            warp[i][0] = temp[i][0];
            warp[i][1] = temp[i][1];

        }
        float ret = (float) globdist[xsize - 1][ysize - 1];
        return ret;
    }
}