package com.acquiring.app.classifier;

import android.support.v4.app.FragmentActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

public class Classifier {
    public int parameters;
    public ArrayList<Symbols> TrainiedData = new ArrayList<Symbols>();
    public DTW dtw = new DTW();

    private int numOfSamples = 1;

    public final pair Classifiy(ArrayList<ArrayList<Float>> Inputdata) {
        float GlobalDistance = (float) DTW.VERY_BIG;
        int id = -1;
        for (int i = 0; i < TrainiedData.size(); i++) {
            float distance = (float) DTW.VERY_BIG;
            for (int j = 0; j < TrainiedData.get(i).refrenceData.size(); j++) {
                distance = Math.min(distance, dtw.dtw(Inputdata, TrainiedData.get(i).refrenceData.get(j), parameters));
            }
            if (distance < GlobalDistance) {
                GlobalDistance = distance;
                id = TrainiedData.get(i).id;
            }
        }

        return new pair(id, GlobalDistance);
    }

    public void trainData(FragmentActivity activity){
        for (int j = 0; j < 6; j++) {
            Symbols s = new Symbols();
            s.id = j;
            for (int i = 1; i <= numOfSamples; i++) {
                String FileName = String.format("accOut_%d_%d.txt", j, i);
                String fullPath = FileName;
                Scanner input = new Scanner(System.in);
                File file = new File(fullPath);
                try {
                    FileInputStream fis = activity.openFileInput(FileName);
                    input = new Scanner(fis);

                    ArrayList<ArrayList<Float>> readedData = new ArrayList<ArrayList<Float>>();
                    int z = 0;
                    while (input.hasNext()) {
                        ArrayList<Float> ins = new ArrayList<Float>();
                        String line = input.nextLine();
                        if (z++ == 0) continue;
                        for (String temp : line.split(",")) {
                            ins.add(Float.parseFloat(temp));
                        }

                        readedData.add(ins);
                    }
                    s.refrenceData.add(readedData);
                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            this.TrainiedData.add(s);
        }
    }
}