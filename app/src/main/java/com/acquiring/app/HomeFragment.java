package com.acquiring.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mbientlab.metawear.AsyncOperation;
import com.mbientlab.metawear.DataSignal;
import com.mbientlab.metawear.Message;
import com.mbientlab.metawear.RouteManager;
import com.mbientlab.metawear.UnsupportedModuleException;
import com.acquiring.app.help.HelpOptionAdapter;
import com.mbientlab.metawear.data.CartesianFloat;
import com.mbientlab.metawear.module.Accelerometer;
import com.mbientlab.metawear.module.Bma255Accelerometer;
import com.mbientlab.metawear.module.Bmi160Accelerometer;
import com.mbientlab.metawear.module.DataProcessor;
import com.mbientlab.metawear.module.Debug;
import com.mbientlab.metawear.module.Gpio;
import com.mbientlab.metawear.module.Led;
import com.mbientlab.metawear.module.Logging;
import com.mbientlab.metawear.module.Mma8452qAccelerometer;
import com.mbientlab.metawear.module.Settings;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;


public class HomeFragment extends ModuleFragmentBase {
    private Led ledModule;
    private Gpio gpioModule;
    private Accelerometer accelModule;
    private Logging logModule;
    private Debug debugModule;
    private Settings settingsModule;

    private byte gpioPin = 0;
    private int rangeIndex= 2;
    private int GPIO_Switch_State = 0, debug_cnt = 0, unknown_entry_cnt = 0;
    private Long tStart, lastPress = System.currentTimeMillis();

    private ArrayList<Message> accData = new ArrayList<>();
    private final Handler handler= new Handler();

    private static final float[] MMA845Q_RANGES= {2.f, 4.f, 8.f}, BMI160_RANGES= {2.f, 4.f, 8.f, 16.f};
    private static final float ACC_FREQ= 50.f;
    private static final String STREAM_KEY= "accel_stream";
    private static final String LOG_TAG = "MainActivity";

    private final AsyncOperation.CompletionHandler<RouteManager> dataStreamManager =
            new AsyncOperation.CompletionHandler<RouteManager>() {
        @Override
        public void success(RouteManager result) {
            result.setLogMessageHandler(STREAM_KEY, new RouteManager.MessageHandler() {
                @Override
                public void process(Message message) {
                    final CartesianFloat spin = message.getData(CartesianFloat.class);
                    Log.i("Sensors_Data", String.format(Locale.US, "x, y, z: %f %f %f %d",
                            spin.x(), spin.y(), spin.z(), message.getTimestamp().getTimeInMillis()));
                    accData.add(message);
                }
            });

            result.subscribe(STREAM_KEY, new RouteManager.MessageHandler() {
                @Override
                public void process(Message message) {
                    final CartesianFloat spin = message.getData(CartesianFloat.class);
                    Log.i("Sensors_Data", String.format(Locale.US, "x, y, z: %f %f %f %d",
                            spin.x(), spin.y(), spin.z(), message.getTimestamp().getTimeInMillis()));
                    accData.add(message);
                }
            });
        }
    };

    public static class MetaBootWarningFragment extends DialogFragment {
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity()).setTitle(R.string.title_warning)
                    .setPositiveButton(R.string.label_ok, null)
                    .setCancelable(false)
                    .setMessage(R.string.message_metaboot)
                    .create();
        }
    }

    public HomeFragment() {
        super(R.string.navigation_fragment_home);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void configureChannel(Led.ColorChannelEditor editor) {
        final short PULSE_WIDTH= 1000;
        editor.setHighIntensity((byte) 31).setLowIntensity((byte) 31)
                .setHighTime((short) (PULSE_WIDTH >> 1)).setPulseDuration(PULSE_WIDTH)
                .setRepeatCount((byte) -1).commit();
    }

    protected String saveData() {
        final String CSV_HEADER = "";//String.format("time,x-%s,y-%s,z-%s%n", dataType, dataType, dataType);
        String filename = String.format("%s_%tY%<tm%<td-%<tH%<tM%<tS%<tL.csv", getContext().getString(sensorResId), Calendar.getInstance());

        try {
            FileOutputStream fos = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(CSV_HEADER.getBytes());

//            LineData data = chart.getLineData();
//            LineDataSet xSpinDataSet = data.getDataSetByIndex(0), ySpinDataSet = data.getDataSetByIndex(1),
//                    zSpinDataSet = data.getDataSetByIndex(2);
//            for (int i = 0; i < data.getXValCount(); i++) {
//                fos.write(String.format("%.3f,%.3f,%.3f,%.3f%n", i * samplePeriod,
//                        xSpinDataSet.getEntryForXIndex(i).getVal(),
//                        ySpinDataSet.getEntryForXIndex(i).getVal(),
//                        zSpinDataSet.getEntryForXIndex(i).getVal()).getBytes());
//            }
            fos.close();
            return filename;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void boardReady() throws UnsupportedModuleException {
        setupFragment(getView());
    }

    @Override
    protected void fillHelpOptionAdapter(HelpOptionAdapter adapter) {

    }

    @Override
    public void reconnected() {
        setupFragment(getView());
    }

    private void data_handler(){
        String filename = String.format("accel.csv");

        final String CSV_HEADER = "";//String.format("time,x-%s,y-%s,z-%s%n", dataType, dataType, dataType);

        Log.i(LOG_TAG , "Acc. Size: " + accData.size());
        Log.i(LOG_TAG , "Unknown Size: " + unknown_entry_cnt);
        try {
            FileOutputStream fos = getActivity().openFileOutput(filename, Context.MODE_PRIVATE);
            fos.write(CSV_HEADER.getBytes());

            for (int i = 0; i < accData.size(); i++) {
                final CartesianFloat spin = accData.get(i).getData(CartesianFloat.class);
                String str = String.format("%f,%f,%f,%d%n", spin.x(), spin.y(), spin.z(), accData.get(i).getTimestamp().getTimeInMillis());
                fos.write(str.getBytes());
            }
            accData.clear();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupFragment(final View v) {
        final String METABOOT_WARNING_TAG= "metaboot_warning_tag";
        if (!mwBoard.isConnected()) {
            return;
        }

        if (mwBoard.inMetaBootMode()) {
            if (getFragmentManager().findFragmentByTag(METABOOT_WARNING_TAG) == null) {
                new MetaBootWarningFragment().show(getFragmentManager(), METABOOT_WARNING_TAG);
            }
        } else {
            DialogFragment metabootWarning= (DialogFragment) getFragmentManager().findFragmentByTag(METABOOT_WARNING_TAG);
            if (metabootWarning != null) {
                metabootWarning.dismiss();
            }
        }

        try {
            logModule = mwBoard.getModule(Logging.class);
            ledModule = mwBoard.getModule(Led.class);
            gpioModule = mwBoard.getModule(Gpio.class);
            accelModule = mwBoard.getModule(Accelerometer.class);
            debugModule = mwBoard.getModule(Debug.class);
            settingsModule = mwBoard.getModule(Settings.class);
        } catch (UnsupportedModuleException e) {
            e.printStackTrace();
        }

        settingsModule.handleEvent().fromDisconnect().monitor(new DataSignal.ActivityHandler() {
            @Override
            public void onSignalActive(Map<String, DataProcessor> processors,
                                       DataSignal.DataToken token) {
                //logModule.clearEntries();
                debugModule.resetAfterGarbageCollect();
            }
        }).commit();

        if (accelModule instanceof Bmi160Accelerometer || accelModule instanceof Bma255Accelerometer) {
            accelModule.setAxisSamplingRange(BMI160_RANGES[rangeIndex]);
        } else if (accelModule instanceof Mma8452qAccelerometer) {
            accelModule.setAxisSamplingRange(MMA845Q_RANGES[rangeIndex]);
        }
        accelModule.setOutputDataRate(ACC_FREQ);
        accelModule.routeData().fromAxes().log(STREAM_KEY).commit().onComplete(dataStreamManager);

        gpioModule.setPinChangeType(gpioPin, Gpio.PinChangeType.ANY);
        gpioModule.setPinPullMode(gpioPin, Gpio.PullMode.PULL_DOWN);
        gpioModule.startPinChangeDetection(gpioPin);

        gpioModule.routeData().fromDigitalInChange(gpioPin).stream("gpio_0_notify_stream").commit()
                .onComplete(new AsyncOperation.CompletionHandler<RouteManager>() {
            @Override
            public void success(RouteManager result) {
                result.subscribe("gpio_0_notify_stream", new RouteManager.MessageHandler() {
                    @Override
                    public void process(Message msg) {
                        int state = msg.getData(Byte.class);
                        Long lastPressTime = System.currentTimeMillis() - lastPress;

                        Log.i(LOG_TAG, String.format(Locale.US,
                                "[TRACE] GPIO: %d, GPIO_Switch_State: %d, lastPressTime: %d",
                                state, GPIO_Switch_State, lastPressTime));

                        if (lastPressTime < 50) return;

                        Log.i(LOG_TAG, String.format(Locale.US, "GPIO: %d, Last Press: %d",
                                state, lastPressTime/1000));

                        if(state != GPIO_Switch_State) {
                            if (GPIO_Switch_State == 0) {
                                accData.clear();
                                unknown_entry_cnt = 0;
                                toggleSensors(true);
                            }else{
                                toggleSensors(false);
                            }
                            Log.i(LOG_TAG, String.format(Locale.US, "------------- %d -------------",
                                    debug_cnt++));

                            lastPress = System.currentTimeMillis();
                        }
                        GPIO_Switch_State = state;
                    }
                });
            }
        });
    }

    private void toggleSensors(Boolean sw){
        if(sw) {
            configureChannel(ledModule.configureColorChannel(Led.ColorChannel.BLUE));
            logModule.startLogging(true);
            accelModule.enableAxisSampling();
            accelModule.start();
            ledModule.play(true);
            //handler.postDelayed(stopLog, 3000);
        }else{
            ledModule.stop(true);
            accelModule.stop();
            accelModule.disableAxisSampling();
            logModule.stopLogging();
            dlLog();
        }
    }

    private final Runnable stopLog= new Runnable() {
        @Override
        public void run() {
            Log.i(LOG_TAG, "stopLog");
            ledModule.stop(true);
            logModule.stopLogging();
            accelModule.stop();
            accelModule.disableAxisSampling();
            dlLog();
        }
    };

    private void dlLog() {
        logModule.downloadLog(0, new Logging.DownloadHandler() {
            @Override
            public void onProgressUpdate(int nEntriesLeft, int totalEntries) {
                Log.i(LOG_TAG, String.format("Progress: %d/%d/", nEntriesLeft, totalEntries));

                if (nEntriesLeft == 0) {
                    Log.i(LOG_TAG, "Log download completed");
                    logModule.clearEntries();
                    data_handler();
                    //debugModule.disconnect();
                    //mwBoard.connect();
                }
            }

            @Override
            public void receivedUnknownLogEntry(byte logId, Calendar timestamp, byte[] data) {
                Log.i("Sensors_Data", String.format("Unknown log entry: {id: %d, data: %s}", logId, Arrays.toString(data)));
                unknown_entry_cnt++;
            }
        });
    }
}
